import axios from "axios";

export const API = axios.create({
    baseURL: "https://labeddit-backend-5efj.onrender.com/"
});